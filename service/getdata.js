const mockData = [
  {
    funds_name: 'B-INCOMESSF',
    nav: '10.0548',
    bid: '10.0549',
    Offer: '10.0548',
    change: '0.0107 ',
  },
  { funds_name: 'BM70SSF', nav: '9.9774', bid: '9.9775', Offer: '9.9774', change: '0.0927' },
  { funds_name: 'BEQSSF', nav: '10.0548', bid: '11.2471', Offer: '11.247', change: '0.1319' },
  {
    funds_name: 'B-FUTURESSF',
    nav: '11.443',
    bid: '11.4431',
    Offer: '11.443',
    change: '0.1488',
  },
]
const fetchData = () => {
  return new Promise((res, rej) => {
    const funds_nav = {
      data: mockData,
    }
    res(funds_nav)
  })
}
module.exports = {
  fetchData,
}
