const html2json = require('html2json').html2json
const process = require('process')
const request = require('request')
const { fetchData } = require('./service/getdata')
const run = () => {
  const fundName = process.argv[2]

  const options = {
    url: 'https://codequiz.azurewebsites.net/',
    headers: {
      'User-Agent': 'request',
      Cookie: 'hasCookie = true',
      'content-type': 'application/json',
    },
  }
  request(options, function (error, response, body) {
    const html = `${body}`

    const tableIndexFirst = html.indexOf('<table>')
    const tableIndexLast = html.lastIndexOf('</table>')
    const tagTableClose = '</table>'.length
    const table = html.substring(tableIndexFirst, tagTableClose + tableIndexLast)

    const jsonTables = html2json(table)
    const tableChild = jsonTables.child[0].child
    if (tableChild && Array.isArray(tableChild)) {
      //map data
    }
    console.log(
      jsonTables.child[0].child.map((e) =>
        e.child.map((c) => {
          console.log(c)
        })
      )
    )
  })

  const getData = async () => {
    try {
      const res = await fetchData()
      if (res && res.data) {
        const { data } = res
        return data.find((e) => e.funds_name.includes(fundName) && console.log(e.nav))
      }
    } catch (error) {
      throw new Error(error)
    }
  }
  getData()
}
run()
